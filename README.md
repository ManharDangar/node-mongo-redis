### Dockorize Nodejs, Mongodb, Redis App, Push images on AWS ECR private repo, Deployed on EC2 Using Jenkins CICD.
-  Create VPC
-  Create EC2 in public subnet
-  Create RDS in private subnet
-  Install Jenkins 
-  Install Sonarqube
-  Install Docker and Docker Composer
-  Creat AWS ECR private registery 

-  Create Dockerfile for Nodejs, Mongodb, Redis.
-  Write the docker compose file
-  Create CICD using Jenkins

![alt text](infra-aws.png)
